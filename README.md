# Tools

This repository contains development tools.

## Dockerfiles

This chapter refers to the content of the `dockerfiles` sub-folder.

Dockerfiles are specification files to create Docker container. Docker containers are a self-contained environment used in the Gitlab pipeline to build, test, generate documentation and quality reports of the Software items.

Docker image are built against the specification (Dockerfile) which ensures the repeatability and reproducibility of the build and test execution. Dockerfile lists the dependencies and actions to create the Docker image.

List of Docker images:

* `python` python3.7 based Docker image for Python projects.
* `rpi` contains all packages to generate the embedded Linux Operating System for the Raspberry Pi target.

### Docker image versioning

Before publishing the Docker image to the registry. The images are tagged with a specific name used by Gitlab to retrieve them when the CI/CD pipeline is triggered.

As for the software source code, the Docker images shall be tagged when a release is created.

During development the latest Docker image can be used `registry.gitlab.com/gofleetsolutions/rpi:latest`. This is the default tag name created by the `build.sh` script.

But once release is created the Docker image shall be tagged with the milestone version. The `DOCKER_IMAGE_*` variable of the `build.sh` script shall be changed accordingly. (ex: `registry.gitlab.com/gofleetsolutions/rpi:v1.0.0`).
Do not forget to reference this tag name in the project CI/CD configuration file: `.gitlab-ci.yml`.

### Build Docker images

The `build.sh` creates and publishes Docker images. Once created the images are published to Docker registry (Docker image container). The Docker registry provides the images to the Gitlab pipeline.

Build `rpi` by running:

```sh
./build.sh rpi
```
