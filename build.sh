#!/bin/bash
# Copyright (C) 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
# Rémi Doumenc remi.doumenc@gmail.com

TAG=${2:-latest}
REGISTRY="registry.gitlab.com/gofleetsolutions/tools"

DOCKER_IMAGE_RPI="${REGISTRY}/rpi:${TAG}"
DOCKER_FILE_RPI="./dockerfiles/rpi-dockerfile"

PYTHON_IMAGE_RPI="${REGISTRY}/python:${TAG}"
PYTHON_FILE_RPI="./dockerfiles/python-dockerfile"


## Build the docker image
function docker_build {
  docker build --no-cache --tag "$1" --file "$2" .
}

## Push the docker image to Docker HUB registry
function docker_push {
  docker push "$1"
}

case "$1" in
  'rpi')
    docker_build ${DOCKER_IMAGE_RPI} ${DOCKER_FILE_RPI}
    docker_push ${DOCKER_IMAGE_RPI}
    exit
    ;;
  'python')
    docker_build ${PYTHON_IMAGE_RPI} ${PYTHON_FILE_RPI}
    docker_push ${PYTHON_IMAGE_RPI}
    exit
    ;;
  *)
    echo "ERROR: unknown parameter $1"
    exit 1
    ;;
esac
